
-- SUMMARY --

* Upload files directly to choosen (token based) folder and allow access to
  Dropbox files for some roles


-- REQUIREMENTS --

* token
* field
* libraries


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.
* Dropbox library:
    - Copy library from https://www.dropbox.com/developers-v1/core/sdks/php (url can be changed over time)
    - Place library to sites/all/libraries
    - Folder structure should be organized sites/all/libraries/Dropbox/autoload.php


-- CONFIGURATION --

* Configure upload access settings on admin/config/content/dropbox-upload/settings
* Configure user permissions on admin/people/permissions#module-dropbox_upload
