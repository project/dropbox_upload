<?php

/**
 * @file
 * {@inheritdoc}
 */

/**
 * {@inheritdoc}
 *
 * @return mixed
 *   Mixed {@inheritdoc}.
 */
function dropbox_upload_access_settings() {

  $form['dropbox_upload_key'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('dropbox_upload_key', ''),
    '#title' => t('Dropbox upload key'),
    '#description' => t('Enter your Dropbox app key. If you do not have an app key, you can create one at <a href="@url" target="_blank">Dropbox developers</a>', array(
      '@url' => 'https://www.dropbox.com/developers/apps/create',
    )),
  );

  $form['dropbox_upload_secret'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('dropbox_upload_secret', ''),
    '#title' => t('Dropbox upload secret'),
    '#description' => t('Enter your Dropbox app secret. If you do not have an app secret, you can create one at <a href="@url" target="_blank">Dropbox developers</a>', array(
      '@url' => 'https://www.dropbox.com/developers/apps/create',
    )),
  );

  if (is_string(variable_get('dropbox_upload_website_token_secret', 0)  &&
    is_string(variable_get('dropbox_upload_website_oauth_token', 0)) &&
    is_string(variable_get('dropbox_upload_website_oauth_token_secret', 0))
  )
  ) {

    $form['dropbox_install_widesite'] = array(
      '#type' => 'item',
      '#markup' => '<span>' . l(t('Link Dropbox to Website', 'dropbox-upload/install')) . '</span>',
    );
  }
  elseif (variable_get('dropbox_upload_key', FALSE) && variable_get('dropbox_upload_secret', FALSE)) {
    $form['dropbox_install_widesite'] = array(
      '#type' => 'item',
      '#markup' => t('Dropbox Upload for Website was configured successfully.') . '<span>' . "  " .
      l(t('Link Dropbox to Website with another account'), 'dropbox-upload/install') . '</span>',
    );
  }

  return system_settings_form($form);
}
