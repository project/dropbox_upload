<?php

/**
 * @file
 * {@inheritdoc}
 */

use dbx\WriteMode;
use dbx\Client;

/**
 * Implements hook_field_info().
 */
function dropbox_upload_field_info() {
  return array(
    'dropbox_upload_file' => array(
      'label' => t('Dropbox Upload'),
      'description' => t('This field stores files on connected Dropbox account'),
      'default_widget' => 'dropbox_upload_widget',
      'default_formatter' => 'dropbox_upload_formatter',
    ),
  );
}

/**
 * Implements hook_field_widget_info().
 */
function dropbox_upload_field_widget_info() {
  return array(
    'dropbox_upload_widget' => array(
      'label' => t('Dropbox upload'),
      'field types' => array('dropbox_upload_file'),
    ),
  );
}

/**
 * Implements hook_permission().
 */
function dropbox_upload_permission() {
  return array(
    'edit dropbox_upload_file field' => array(
      'title' => t('Edit Dropbox upload field'),
      'description' => t('Allow users to access config page'),
    ),
    'view dropbox_upload_file field' => array(
      'title' => t('View Dropbox upload field output'),
      'description' => t('Allow users to see field output'),
    ),
  );
}

/**
 * Implements hook_field_access().
 */
function dropbox_upload_field_access($op, $field, $entity_type, $entity, $account) {

  if ($field['type'] == 'dropbox_upload_file' && $op == 'edit') {
    return user_access('edit dropbox_upload_file field', $account);
  }
  if ($field['type'] == 'dropbox_upload_file' && $op == 'view') {
    return user_access('view dropbox_upload_file field', $account);
  }
  return TRUE;
}

/**
 * Implements hook_field_widget_form().
 */
function dropbox_upload_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  if ($field['cardinality'] == 1) {
    $element['#type'] = 'fieldset';
  }

  // Print allowed extensions if they are set on field settings.
  $ext = !empty($instance['widget']['settings']['file_extensions']) ? t('Allowed extensions: @ext', array('@ext' => $instance['widget']['settings']['file_extensions'])) : t('All extensions are allowed.');
  // File fiels.
  $element['dropboxfile'] = array(
    '#type' => 'managed_file',
    '#title' => t('Dropbox upload File'),
    '#description' => t('Choose files to upload to connected Dropbox account. @ext', array('@ext' => $ext)),
    '#upload_location' => 'public://dropbox',
  );
  // Path field (visible only on field settings page)
  $node = menu_get_object();
  $default_path = '';
  if (!is_object($node)) {
    if (isset($instance['default_value'][0]['dropboxpath']) && !empty($instance['default_value'][0]['dropboxpath'])) {
      $default_path = $instance['default_value'][0]['dropboxpath'];
    }
  }
  else {
    $default_path = '';
  }

  $element['dropboxpath'] = array(
    '#type' => 'textfield',
    '#title' => t('Dropbox upload Path'),
    '#description' => t('Configure where the file go in Dropbox, using tokens, for example: interventions/[current-date:year]/[current-date:month]/[node:title] in order to keep it clean. The filename can be kept as is.'),
    '#access' => FALSE,
    '#default_value' => $default_path,
  );
  // Token field (visible only on field settings page)
  $element['dropboxtokens'] = array(
    '#theme' => 'token_tree_link',
    '#token_types' => array('node'),
    '#global_types' => TRUE,
    '#click_insert' => TRUE,
    '#access' => FALSE,
  );

  return $element;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function dropbox_upload_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id) {
  // Show path field on field config page.
  if (isset($form['#field']['type']) && $form['#field']['type'] == 'dropbox_upload_file') {
    $field_name = $form['#field']['field_name'];
    $form['instance']['default_value_widget'][$field_name]['und'][0]['dropboxpath']['#access'] = TRUE;
    $form['instance']['default_value_widget'][$field_name]['und'][0]['dropboxtokens']['#access'] = TRUE;
  }
}

/**
 * Implements hook_field_is_empty().
 */
function dropbox_upload_field_is_empty($item, $field) {
  if (empty($item['dropboxpath'])) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_field_widget_settings_form().
 */
function dropbox_upload_field_widget_settings_form($field, $instance) {
  $element['file_extensions'] = array(
    '#type' => 'textfield',
    '#title' => t('Allowed file extensions'),
    '#default_value' => isset($instance['widget']['settings']['file_extensions']) ? str_replace(' ', ', ', $instance['widget']['settings']['file_extensions']) : '',
    '#description' => t('Separate extensions with a space or comma and do not include the leading dot.'),
    '#element_validate' => array('_file_generic_settings_extensions'),
    '#maxlength' => 256,
  );

  $element['max_filesize'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum upload size'),
    '#default_value' => isset($instance['widget']['settings']['max_filesize']) ? $instance['widget']['settings']['max_filesize'] : '',
    '#description' => t('Enter a value like "512" (bytes), "80 KB" (kilobytes) or "50 MB" (megabytes) in order to restrict the allowed file size. If left empty the file sizes will be limited only by PHP\'s maximum post and file upload sizes (current limit <strong>%limit</strong>).', array('%limit' => format_size(file_upload_max_size()))),
    '#size' => 10,
    '#element_validate' => array('_file_generic_settings_max_filesize'),
  );

  return $element;
}

/**
 * Implements hook_field_validate().
 */
function dropbox_upload_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  // Get current node.
  $node = menu_get_object();
  // Check if there is our field.
  $field_name = '';
  if (!empty($node) || is_object($node)) {
    if ($field['type'] == 'dropbox_upload_file') {
      $field_name = $field['field_name'];
    }
  }
  foreach ($items as $delta => $item) {
    if ($item['dropboxfile'] == 0) {
      continue;
    }
    $file = file_load($item['dropboxfile']);
    if (isset($instance['default_value'][0]['dropboxpath'])) {
      if (!empty($field_name)) {
        // Set error messages.
        $parse = isset($instance['widget']['settings']['max_filesize']) ? parse_size($instance['widget']['settings']['max_filesize']) : '';
        if (!empty($instance['widget']['settings']['max_filesize']) && !is_numeric($parse)) {
          $errors[$field['field_name']][$langcode][$delta][] = array(
            'error' => 'max_filesize',
            'message' => t('This option must contain a valid value. You may either leave the text field empty or enter a string like "512" (bytes), "80 KB" (kilobytes) or "50 MB" (megabytes).'),
          );
        }
        if (!empty($instance['widget']['settings']['max_filesize']) && is_numeric($parse)) {
          if ($parse < $file->filesize) {
            $errors[$field['field_name']][$langcode][$delta][] = array(
              'error' => 'max_filesize',
              'message' => t('File is too big for uploading. Please check filesize settings'),
            );
          }
        }
        if (!empty($instance['widget']['settings']['file_extensions'])) {
          $extensions = preg_replace('/([, ]+\.?)/', ' ', trim(strtolower($instance['widget']['settings']['file_extensions'])));
          $extensions = array_filter(explode(' ', $extensions));
          $extensions = implode(' ', array_unique($extensions));
          if (!preg_match('/^([a-z0-9]+([.][a-z0-9])* ?)+$/', $extensions)) {
            $errors[$field['field_name']][$langcode][$delta][] = array(
              'error' => 'file_extensions',
              'message' => t('The list of allowed extensions is not valid, be sure to exclude leading dots and to separate extensions with a comma or space.'),
            );
          }
          $name = !empty($file->filename) ? $file->filename : '';
          $ext = pathinfo($name, PATHINFO_EXTENSION);
          if (!empty($ext) && strpos($extensions, $ext) == FALSE) {
            $errors[$field['field_name']][$langcode][$delta][] = array(
              'error' => 'file_extensions',
              'message' => t('Extension ".@ext" is not allowed. Please choose one of allowed ones.', array('@ext' => $ext)),
            );
          }
        }

        // Process Dropbox upload if there are no errors and.
        // file is uploaded to Drupal.
        $name = !empty($file->filename) ? $file->filename : '';
        if (empty($errors) && !empty($name)) {
          // Replace tokens to make readable path.
          $path = token_replace($instance['default_value'][0]['dropboxpath'], array('node' => $node));
          $path = str_replace(' ', '-', $path);
          // Upload file to dropbox.
          $accessToken = variable_get('dropbox_upload_access_token', FALSE);
          $dbxClient = new Client($accessToken, "Dropbox/1.0");
          $f = fopen(drupal_realpath($file->uri), "rb");
          $result = $dbxClient->uploadFile('/' . $path . '/' . $name, WriteMode::add(), $f);
          fclose($f);
          // Save file data in database.
          if (!empty($name) && !empty($path)) {
            // Check if there is filename on current nodelready.
            $filename_exists = db_select('dropbox_files', 'df')
              ->fields('df', array('filename', 'id'))
              ->condition('entity_id', $node->nid)
              ->condition('path', $path)
              ->condition('filename', $name)
              ->execute()
              ->fetchField();
            // Write new file in db if it's not already there.
            if (!$filename_exists) {
              $file_data = db_insert('dropbox_files')
                ->fields(array(
                  'entity_id' => $node->nid,
                  'filename' => $name,
                  'path' => $path,
                  'file_delta' => $delta,
                  'timestamp' => time(),
                ))
                ->execute();
            }
          }
          // Delete file when process is done.
          file_delete($file);
          // Get message about dropbox file uploading.
          if (isset($result['error'])) {
            drupal_set_message($result->error);
          }
          else {
            drupal_set_message('File is successfully uploaded to Dropbox', 'status');
          }
        }
      }
    }
  }
}

/**
 * Implements hook_field_formatter_info().
 */
function dropbox_upload_field_formatter_info() {
  return array(
    'dropbox_upload_formatter' => array(
      'label' => t('Dropbox upload'),
      'field types' => array('dropbox_upload_file'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function dropbox_upload_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, &$items, $display) {
  $element = array();
  $node = menu_get_object();
  switch ($display['type']) {
    case 'dropbox_upload_formatter':
      // Get all files from current node.
      $filenames = db_select('dropbox_files', 'df')
        ->distinct('df.filename')
        ->fields('df', array('filename', 'id'))
        ->condition('entity_id', $node->nid)
        ->execute()
        ->fetchAll();
      foreach ($filenames as $key => $filename) {
        // Create dbx path.
        $path = token_replace($instance['default_value'][0]['dropboxpath'], array('node' => $node));
        $path = str_replace(' ', '-', $path);
        $full_path = $path . '/' . $filename->filename;
        // Get shareable link.
        $accessToken = variable_get('dropbox_upload_access_token', FALSE);
        $dbxClient = new Client($accessToken, "Dropbox/1.0");
        $file_url = $dbxClient->createShareableLink('/' . $full_path);
        $link = !empty($file_url) ? l(t('Get "@name" from Dropbox', array('@name' => $filename->filename)), $file_url, array('attributes' => array('target' => '_blank'))) : t("Dropbox file isn\'t available");
        if (empty($file_url)) {
          db_delete('dropbox_files')->condition('id', $filename->id)->execute();
        }
        $element[$key] = array(
          '#type' => 'markup',
          '#markup' => $link,
        );
      }
      break;
  }
  // Remove duplicates (if any)
  $element = array_map("unserialize", array_unique(array_map("serialize", $element)));
  foreach ($element as $value) {
    $items[]['link'] = $value['#markup'];
  }

  return $element;
}
